<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

 public	function __construct()
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->helper('html');
			$this->load->model("M_data");
			$this->load->model("product_m");
			
			
		}

	public function index()
	{
		$this->home();
	}

	public function home()
	{

		$this->load->view('halamanutama');

	}

	public function load()
	{
		$produk = $this->product_m->load();
		$data = array('produk' =>$produk  );
		$this->load->view('tampil',$data);
	}

	public function product()
	{


		$this->load->view('v_produk');
	}

	public function contact()
	{

		$this->load->view('v_contact');
	}

	public function kategori()
	{

		$this->load->view('v_kategori');
	}

	public function cart(){


		$this->load->view('v_cart');
	}

	public function checkout(){

		$this->load->view('v_checkout');
	}

	public function tshirt(){
	//	$data['products'] = $this->M_data->ambil_data()->result();	
	//	$this->load->view('v_tshirt',$data);
	//	 $this->load->view('v_tshirt');
	//	$produk =$this->M_data->ambil_data();
	//	$data = array ('produk' =>$produk);
	//	$this->load->view('v_tshirt',$data);
		}



		
	


	public function shirt(){

		// $data['gambar'] = $this->GambarModel->view();
		$this->load->view('v_shirt');
	}

	public function accessories(){


		$this->load->view('v_accessories');
	}


	public function pants(){


		$this->load->view('v_pants');
	}

	public function order(){


		$this->load->view('v_howtoorder');
	}

	public function gallery(){


		$this->load->view('v_gallery');
	}

}
