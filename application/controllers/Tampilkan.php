<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tampilkan extends CI_Controller {

 public function __construct()
        {
                parent::__construct();
                $this->load->model("product_m");
        }
	public function index()
	{
		$produk = $this->product_m->ambil_data();
		$data = array('produk' =>$produk  );
		$this->load->view('v_allproduk',$data);
	}
	
}
