<?php defined('BASEPATH') OR exit('No direct Script access allowed');
class EmailController extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
    }
    public function index() {
        $this->load->helper('form');
        $this->load->view('contact_email_form');
    }
    public function send_mail() {
        $from_email = $this->input->post('email');
       // $from_email = $this->input->post('email');
        $to_email = "prasetiawanilham@gmail.com";
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');
        $nama = $this->input->post('nama');
        //Load email library
        $this->load->library('email');
        $this->email->from($from_email, $nama);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
        //Send mail
        //if($this->email->send())
        //    $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
//        else
  //          $this->session->set_flashdata("email_sent","You have encountered an error");
        $this->load->view('v_contact');
    }
}
