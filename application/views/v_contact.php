<!DOCTYPE html>
<html lang="zxx">
<head>
	 <?php	$this->load->view('headermenu/head.php'); ?>

	
</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
    <?php $this->load->view('headermenu/header.php'); ?>
	<!-- Header section end -->


	<!-- Page info -->
	<?php $this->load->view('kontak/pageinfokontak.php'); ?>
	<!-- Page info end -->


	<!-- Contact section -->
	
	<?php $this->load->view('kontak/contactsection.php'); ?>
	<!-- Contact section end -->


	<!-- Related product section -->
	<br>
	<!-- Related product section end -->


	<!-- Footer section -->
	<?php $this->load->view('foter/footer.php'); ?>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<?php $this->load->view('foter/script.php'); ?>

	</body>
</html>
