<!DOCTYPE html>
<html lang="zxx">
<head>

<?php $this->load->view('headermenu/head.php'); ?>

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<?php $this->load->view('headermenu/header.php'); ?>
	<!-- Header section end -->


	<!-- Page info -->
	<div class="page-top-info">
		<div class="container">
			<h4>All product</h4>
			<div class="site-pagination">
				<a href="">Home</a> /
				<a href="">Shop</a> /
			</div>
		</div>
	</div>
	<!-- Page info end -->


	<!-- Category section -->
	<?php $this->load->view('kategori/categorysection'); ?>
	<!-- Category section end -->


	<!-- Footer section -->
	<?php $this->load->view('foter/footer.php'); ?>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<?php $this->load->view('foter/script.php'); ?>
	</body>
</html>
