<!--blog : https://johansantri.blogspot.com/ -->
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	 <?php $this->load->view('headermenu/head.php'); ?>
	 <?php $this->load->view('headermenu/header.php'); ?>
	
	<script >
		$(document).ready(function(){
			var count = 0;
			var limit=3;
			var test="welcome/load";
			$("button").click(function(){
				limit=limit + 3;
			$("#page").load(test,{
				tambah:limit				
			
			});	

			if(limit == test.length){
				count = 0;
				$("#btn").hide();
				$("#akhir").append("");
				return;
			}

			});
			
		});
	</script>
</head>
<body>
	<!-- Page info -->
	<?php $this->load->view('tshirt/tshirtpageinfo.php'); ?>
	<!-- Page info end -->
	&nbsp;
    &nbsp;
    <div class="container">
		<?php echo "[TAMBAHKAN DISINI] FITUR SEARCH DAN SORT BY DATE!"; ?>
    	<div class="title" style="text-align: center">
    		<h2>Product List</h2>
    	</div>
    	
    	<hr>
	 <div class="row" id="page">
	 <?php foreach ($produk as $product) : ?>
	
  <div class="col-lg-4 col-sm-6" >
  	<br>
    <div class="product-item"  >
    	<div class="card" style="text-align: center;"> 
	      <div class="pi-pic">        
           <img src="<?php echo base_url()?>upload/product/<?php echo $product->image ?>"   alt="" class="img-responsive image1">        
           </div>
		    &nbsp;
            <h5 style="min-height:8px;"><?php echo $product->name?></h5>
            <p><large><?php echo $product->description ?></large></p>
		    <p><strong>IDR.<?php echo number_format($product->price) ?>K</strong></p>

        
      
          </div>
          </div>
            </div>
   
  <?php endforeach; ?>
  
</div>

</div> 
<br>
	<div class="container">
		<div style="text-align: center;">
		
<button class="site-btn sb-line sb-dark" id="btn">load more</button>
<span style="color: black" id="akhir"></span>
</div>
</div>
 &nbsp;
 &nbsp;
 &nbsp;

 <?php $this->load->view('foter/footer.php'); ?>
 <?php $this->load->view('foter/script.php'); ?>
</body>
</html>