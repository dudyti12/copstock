<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
   <?php $this->load->view('headermenu/head.php'); ?>
	
	
	
</head>
<body>
	
    <div class="container"  >

	 <div class="row" id="page">

	 <?php foreach ($produk as $product) : ?>
	
  <div class="col-lg-4 col-sm-6" >
    <br>
    <div class="product-item"  >
     <div class="card" style="text-align: center;"> 
	 <div class="pi-pic">        
      <img src="<?php echo base_url()?>upload/product/<?php echo $product->image ?>"   alt="" class="img-responsive image1">        
    </div>
    &nbsp;
        <h5 style="min-height:8px;"><?php echo $product->name?></h5>
        <p><large><?php echo  $product->description?></large></p>
		<p><strong>IDR.<?php echo  number_format($product->price)?>K</strong></p>
        
     
    </div>
  </div>
  </div>
   
  <?php endforeach; ?>

</div>

</div> 

<?php $this->load->view('foter/script.php'); ?>
</body>
</html>