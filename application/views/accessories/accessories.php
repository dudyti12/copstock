<section class="category-section spad">
		<div class="container">
			<div class="row">
				
					
					
					
				</div>

				<div class="col-lg-9  order-1 order-lg-2 mb-5 mb-lg-0">
					<div class="row">
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<div class="tag-sale">ON SALE!</div>
									<img src="<?php echo base_url();?>assets/accessories/1.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.250K</h6>
									<p>Vans Authentic Vibrant Yellow|Size:28cm</p>

								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/2.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.300K</h6>
									<p>Vans Slip on Full Black | Size:28.5cm</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/4.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.300K</h6>
									<p>Vans Old Skool Leather True White|Size:28cm </p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/3.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.650K</h6>
									<p>Vans authentic Palm Spring |Size:23.5cm</p>

								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/5.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.130K</h6>
									<p>Vintage Vans Nylon Sweater|Size:XL</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/6.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.130K</h6>
									<p>Patagonia Women’s H2no Rain Jacket |Size:S </p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/7.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.130K</h6>
									<p>Patagonia Women’s H2no Rain Jacket |Size:S</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/8.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<div class="tag-sale">SOLD OUT!</div>
									<h6>---</h6>
									<p>Converse Chuck Taylor 70’s|Size:25.5cm</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/9.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.500K</h6>
									<p>Reebok Classics Club C85 - White Gum | Size:26cm </p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
							<!--		<div class="tag-new">new</div> -->
									<img src="<?php echo base_url();?>assets/accessories/10.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR>500K</h6>
									<p>Reebok Classics Club C85 - White Gum | Size:26cm</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/11.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.22.936K</h6>
									<p>Authentic Prada Nylon Laptop Bag </p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo base_url();?>assets/accessories/12.jpg" alt="">
									
								</div>
								<div class="pi-text">
									<h6>IDR.22.936K</h6>
									<p>Authentic Prada Nylon Laptop Bag</p>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>