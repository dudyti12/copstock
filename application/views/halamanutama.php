<!DOCTYPE html>
<html lang="zxx">
<head>
		
 <?php	$this->load->view('headermenu/head.php'); ?>


</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<?php $this->load->view('headermenu/header.php'); ?>
	

	<!-- Header section end -->


	<!-- Headline produk section -->
	<?php $this->load->view('utama/headlineproduk.php'); ?>
	<!-- Headline produk end -->


	<!-- Features section -->
	
	<!-- Features section end -->
	<?php $this->load->view('utama/feature.php'); ?>

	<!-- letest product section -->
	<?php $this->load->view('utama/latestproduct.php'); ?>
	<!-- letest product section end -->



	<!-- Product filter section -->
	<?php $this->load->view('utama/topsell.php'); ?>
	<!-- Product filter section end -->


	<!-- Banner section -->
	<?php $this->load->view('utama/banner.php'); ?>
	<!-- Banner section end  -->


	<!-- Footer section -->
	<?php $this->load->view('foter/footer.php'); ?>
	<!-- Footer section end -->

	 <!--====== Javascripts & Jquery ======-->
	 <?php $this->load->view ('foter/script.php'); ?>

	 

	</body>
</html>
