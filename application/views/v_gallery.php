<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html lang="zxx">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/main.css">
<?php $this->load->view('headermenu/head.php'); ?>

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<?php $this->load->view('headermenu/header.php'); ?>
	<!-- Header section end -->


	<!-- Page info -->
	<?php $this->load->view('gallery/gallerypageinfo.php'); ?>
	<!-- Page info end -->


	<!-- Category section -->
	

	<?php $this->load->view('gallery/gallery.php'); ?>
 	



	<!-- Category section end -->


	<!-- Footer section -->
	<?php $this->load->view('foter/footer.php'); ?>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<?php $this->load->view('foter/script.php'); ?>
	</body>
</html>
