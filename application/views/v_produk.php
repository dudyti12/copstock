<!DOCTYPE html>
<html lang="zxx">
<head>
	
 <?php $this->load->view('headermenu/head.php'); ?>
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<?php $this->load->view('headermenu/header.php'); ?>
	<!-- Header section end -->


	<!-- Page info -->
	<?php $this->load->view('Product/pageinfo.php'); ?>
	<!-- Page info end -->


	<!-- product section -->
	<?php $this->load->view('Product/productsection.php'); ?>
	<!-- product section end -->


	<!-- RELATED PRODUCTS section -->
	<?php $this->load->view('Product/relatedproducts.php'); ?>
	<!-- RELATED PRODUCTS section end -->


	<!-- Footer section -->
	
	<?php $this->load->view('foter/footer.php'); ?>
	
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<?php $this->load->view('foter/script.php'); ?>

	</body>
</html>
