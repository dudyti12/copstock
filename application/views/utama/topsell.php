<section class="product-filter-section">
		<div class="container">
			<div class="section-title">
				<h2>TOP SELLING PRODUCTS</h2>
			</div>
			<ul class="product-filter-menu">
		<!--		<li><a href="#">TOPS</a></li>
				<li><a href="#">JUMPSUITS</a></li>
				<li><a href="#">LINGERIE</a></li>
				<li><a href="#">JEANS</a></li>
				<li><a href="#">DRESSES</a></li>
				<li><a href="#">COATS</a></li>
				<li><a href="#">JUMPERS</a></li>
				<li><a href="#">LEGGINGS</a></li> -->
			</ul>
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="<?php echo base_url();?>assets/tshirt/10.jpg" alt="">
							
						</div>
						<div class="pi-text">
							<h6>IDR.90K</h6>
							<p>Godzilla Imax T-Shirt | Size : L </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							
							<img src="<?php echo base_url();?>assets/tshirt/9.jpg" alt="">
							
						</div>
						<div class="pi-text">
							<h6>IDR.90K</h6>
							<p>Twillight Saga New Moon T-Shirt | Size : S</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="<?php echo base_url();?>assets/shirt/10.jpg" alt="">
							
						</div>
						<div class="pi-text">
							<h6>IDR.180K</h6>
							<p>Champion S700 Hoodie EcoSmart Pullover|Size : XL </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="<?php echo base_url();?>assets/shirt/9.jpg" alt="">
							
						</div>
						<div class="pi-text">
							<h6>IDR.110K</h6>
							<p>Even Youth Shirt - Storytelling|Size: XL </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="<?php echo base_url();?>assets/pants/10.jpg" alt="">
							
						</div>
						<div class="pi-text">
							<h6>IDR.150K</h6>
							<p>Charhartt Ruck Single Knee Short Pants|Size:32 </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="<?php echo base_url();?>assets/pants/9.jpg" alt="">
							
						</div>
						<div class="pi-text">
							<h6>IDR.150K</h6>
							<p>Charhartt Ruck Single Knee Short Pants|Size:32</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="<?php echo base_url();?>assets/pants/8.jpg" alt="">
							
						</div>
						<div class="pi-text">
							<h6>IDR.150K</h6>
							<p>Ikea Trousers Mens Sales Pants - Blue Navy | Size:31-32 </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="<?php echo base_url();?>assets/pants/7.jpg" alt="">
							
						</div>
						<div class="pi-text">
							<h6>IDR.150</h6>
							<p>Ikea Trousers Mens Sales Pants - Blue Navy | Size:31-32 </p>
						</div>
					</div>
				</div>
			</div>
			<div class="text-center pt-5">
				
			</div>
		</div>
	</section>