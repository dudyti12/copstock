<section class="top-letest-product-section">
		<div class="container">
			<div class="section-title">
				<h2>OUR LATEST PRODUCTS!</h2>
				<h3>GRAB IT NOW!!</h3>
			</div>
			<div class="product-slider owl-carousel">
				<div class="product-item">
					<div class="pi-pic">
						<img src="<?php echo base_url();?>assets/tshirt/1.jpg" alt="">
						
					</div>
					<div class="pi-text">
						<h6>IDR.90K</h6>
						<p> Vintage Los Angeles Laker T-Shirt | Size: M</p>
					</div>
				</div>
				<div class="product-item">
					<div class="pi-pic">
						
						<img src="<?php echo base_url();?>assets/shirt/1.jpg" alt="">
						
					</div>
					<div class="pi-text">
						<h6>IDR.90K</h6>
						<p>Supercross-England Pre-'75' Classic Motorcross | Size: S</p>
					</div>
				</div>
				<div class="product-item">
					<div class="pi-pic">
						<img src="<?php echo base_url();?>assets/tshirt/2.jpg" alt="">
						
					</div>
					<div class="pi-text">
						<h6>IDR.90K</h6>
						<p>The Beatles T-Shirt | Size : M </p>
					</div>
				</div>
				<div class="product-item">
						<div class="pi-pic">
							<img src="<?php echo base_url();?>assets/pants/1.jpg" alt="">
						
						</div>
						<div class="pi-text">
							<h6>IDR.210K</h6>
							<p>Versace Jeans Coututre - Khaki | Size :32 </p>
						</div>
					</div>
				<div class="product-item">
						<div class="pi-pic">
						
							<img src="<?php echo base_url();?>assets/pants/2.jpg" alt="">
							
						</div>
						<div class="pi-text">
							<h6>IDR.150K</h6>
							<p>Carhartt Carpenter Hickory Stripe Short Pants | Size:32 </p>
						</div>
					</div>
			</div>
		</div>
	</section>