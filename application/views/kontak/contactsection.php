<section class="contact-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 contact-info">
					<h3>Get in touch</h3>
					<p>Jl.Asakota No.32 Taman Seruni,Ampenan (Lombok,Indonesia)</p>
					<p>(+62) 085954493243</p>
					<p>prasetiawanilham@gmail.com</p>
					<div class="contact-social">
						<a href="#"><i class="fa fa-pinterest"></i></a>
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-dribbble"></i></a>
						<a href="#"><i class="fa fa-behance"></i></a>
					</div>
					
					<form method="post" action="http://localhost/copstock/EmailController/send_mail" class="contact-form">
						<input type="text" name="nama" placeholder="Your Name" >
						<input type="text" name="email" placeholder="Your e-mail">
						<input type="text" name="subject" placeholder="Subject">
						<textarea name="message" placeholder="Message"></textarea>
						<button class="site-btn">SEND NOW</button>
					
					</form>
				</div>
			</div>
		</div>
		<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d401.3213524523053!2d116.08323186955933!3d-8.58162842964037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dcdc07c09363173%3A0x8fb1b611f87d6ce2!2sJl.%20Asakota%2C%20Taman%20Sari%2C%20Kec.%20Ampenan%2C%20Kota%20Mataram%2C%20Nusa%20Tenggara%20Bar.!5e1!3m2!1sid!2sid!4v1582558900835!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe></div>
	</section>

	