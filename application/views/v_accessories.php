<!DOCTYPE html>
<html lang="zxx">
<head>

<?php $this->load->view('headermenu/head.php'); ?>

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<?php $this->load->view('headermenu/header.php'); ?>
	<!-- Header section end -->


	<!-- Page info -->
	<?php $this->load->view('accessories/accessoriespageinfo.php'); ?>
	<!-- Page info end -->


	<!-- Category section -->
	

	<?php $this->load->view('accessories/accessories.php'); ?>
 	



	<!-- Category section end -->


	<!-- Footer section -->
	<?php $this->load->view('foter/footer.php'); ?>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<?php $this->load->view('foter/script.php'); ?>
	</body>
</html>
